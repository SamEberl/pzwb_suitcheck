mm# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 22:20:48 2021

@author: Stefan
"""

import random, shutil
from pathlib import Path

path = Path("F:\Downloads\cases.jsonl\out2")

out_path = path/"Alexia.zip"

def zipRandomFileSample(path, out_path, size = 40):
    files = list(path.iterdir())
    sample = random.choices(files, k=size)
    tmp = path/"tmp"
    tmp.mkdir()
    for s in sample:
        shutil.copyfile(s, tmp/s.name)
    shutil.make_archive(".".join(str(out_path).rsplit(".")[:-1]), 'zip', tmp)
    shutil.rmtree(tmp)