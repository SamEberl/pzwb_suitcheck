# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 19:56:52 2021

@author: Stefan
"""

from pathlib import Path
import json, pathlib

import webbrowser
def openCaseHTML(case_id):
    webbrowser.open("https://de.openlegaldata.io/c/"+str(case_id))

p = Path("F:\Downloads\cases.jsonl") #directory containing cases.jsonl

pout = p/"labelled_cases_v0.1"
pout.mkdir(exist_ok = True, parents = True)

manualExcludedCases = [319475, 317976, 124385, 317976, 178107, 83402]

def excludeCase(d):
    manual = 'id' in d.keys() and d['id'] in manualExcludedCases
    try:
        straf = (d['file_number'].split(" ")[1][-1] == "s")
    except:
        straf = False
    urteil = 'type' in d.keys() and d['type'] in ["Urteil", "Endurteil"]
    jurisdic = 'court' in d.keys() and 'jurisdiction' in d['court'].keys() and d['court']['jurisdiction'] in ["Ordentliche Gerichtsbarkeit", "Arbeitsgerichtsbarkeit"]
    return manual or straf or (not urteil) or (not jurisdic)

sentenceGroup = "[\w\s\d,:;-äöü\(\)\/–]*"
textGroup = "[\w\s\d,:;-äöü\(\)\/–\-.€]{1,200}"

import re

def listToOptionGroup(l):
    group = "(?:"
    for s in l:
        group += "(?:"+s+")|"
    return group[:-1] + ")"

lOG = listToOptionGroup

kläger = lOG(["Kläger", "Klägers", "Klägerin", "Klägern", "Verfügungskläger", "Verfügungsklägerin", "Verfügungsklägers",
          "klagende Partei", "klagenden Partei", "Klagepartei", "Antragsteller", "Antragstellerin", "Verfügungsklbger",
          "Klagende", "Klagenden", "Aufhebungsklägerin", "Klägerinnen", "Revisionsklägerin", "Revisionskläger"])+"(?: als Gesamtschuldner)?"+"(?: als Gesamtschuldnerinnen)?"
beklagte = lOG(["Beklagte", "Verfügungsbeklagte", "beklagte Partei", 
            "Beklagepartei", "Beklagten", "Antragsgegner", "Antragsgegnerin",
            "Beklagten", "Verfügungsbeklagten", "beklagten Partei",
            "Drittwiderbeklagte", "Drittwiderbeklagten", "beklagte Land", "beklagten Landes", "beklagten Amtes",
            "Beklagte und Widerklägerin", "Beklagte und Widerkläger"])+"(?: als Gesamtschuldner)?"+"(?: zusammen)?"

trägt = lOG(['tragen', 'trägt', "übernehmen", "übernimmt"])+"(?: auch)*"+"(?: als Gesamtschuldner)*"
kosten = "(?:weiteren )*"+"(?:übrigen )*"+lOG(['Kosten', 'Verfahrenskosten', 'Gerichtskosten'])
der = lOG(['der', 'die', 'das', 'Der', 'Die', 'Das', 'dem', 'den', 'Dem', 'Den', 'des'])
hat = lOG(['hat', 'haben', 'sind von'])+"(?: auch)*"+"(?: wie Gesamtschuldner)?"
verfahrens = "(?:gesamten )?"+"(?:weiteren )?"+"(?:arbeitsgerichtlichen )?"+lOG(['Verfahrens', 'Rechtsstreits', 'Rechtsstreites', 
                                   "einstweiligen Verfügungsverfahrens", "Verfügungsverfahrens", 'Berufungsverfahrens', 
                                   "Erlassverfahrens", "Aufhebungsverfahrens",
                                   "Rechtstreits", "Rechtsstreites", "Rechtsstreit", "Revisionsverfahrens", "Berufung", "Berufungen",
                                   "Revision", "Berufungs- und des Revisionsverfahrens"])
ihre = lOG(['ihre', 'seine'])

berufung = lOG(['Berufung', 'Revision', 'Berufungen', 'Revisionen', 'Anschlussrevision'])

trägtDieKostenDesVerfahrens = lOG([fr'{trägt} die {kosten} {der} {verfahrens}', 
                                   fr'{hat}(?: als Gesamtschuldner)* {der} {kosten} {der} {verfahrens}{sentenceGroup}zu (?:tragen)|(?:zahlen)',
                                   fr"werden {der} {kosten} des {verfahrens} auferlegt"])
trägtDerKläger = lOG([fr'{trägt} {der} {kläger}', fr'{hat} {der} {kläger} zu tragen',
                      fr'werden {der} {kläger} auferlegt',
                      fr"fallen {der} {kläger} zur Last"])
trägtDerBeklagte = lOG([fr'{trägt} {der} {beklagte}', fr'{hat} {der} {beklagte} zu tragen',
                        fr'werden {der} {beklagte} auferlegt'])


zurückgewiesen = r"(?:mit der Maßgabe )?"+lOG(["zurückgewiesen", "als unzulässig verworfen", "z u r ü c k g e w i e s e n", "zurück-gewiesen",
                                              "verworfen", "abgewiesen"])

wird = lOG(["wird", "werden"])+r"(?: mit der Maßgabe)?"

urteil = lOG(["Urteil", "Urteile", "Teilurteil", "Teil-Urteil", "Teil-Anerkenntnis- und Teilendurteil"])

klägerKostenSentences = [fr"{der} {kläger} {trägtDieKostenDesVerfahrens}", 
                         fr"{der} {kosten} {der} {verfahrens}{sentenceGroup}{trägtDerKläger}",
                         fr"Im Übrigen {hat} {der} {kläger} {der} {kosten} des {verfahrens} zu tragen",
                         fr"{der} {kosten} beider (?:Verfahren)|(?:Rechtszüge) {trägt} {der} {kläger}",
                         fr"{der} {kläger} {hat} {der} {kosten} beider (?:Verfahren)|(?:Rechtszüge) zu tragen",
                         fr"{der} {berufung} {der} {kläger} gegen {der} {urteil}{textGroup} {wird} auf {ihre} Kosten {zurückgewiesen}",
                         fr"{der} {berufung} {der} {kläger} gegen {der} {urteil}{textGroup} {wird} auf Kosten {der} {kläger} {zurückgewiesen}",
                         fr"{der} {berufung} {der} {kläger} gegen {der} {urteil}{textGroup} {wird} kostenpflichtig {zurückgewiesen}",
                         fr"{der} {berufung} {wird} auf Kosten {der} {kläger} {zurückgewiesen}"]
beklagteKostenSentences = [fr"{der} {beklagte} {trägtDieKostenDesVerfahrens}", 
                         fr"{der} {kosten} {der} {verfahrens}{sentenceGroup}{trägtDerBeklagte}",
                         fr"{der} {berufung} {der} {beklagte} gegen {der} {urteil}{textGroup} {wird} auf {ihre} Kosten {zurückgewiesen}",
                         fr"{der} {berufung} {der} {beklagte} gegen {der} {urteil}{textGroup} {wird} auf Kosten {der} {beklagte} {zurückgewiesen}",
                         fr"{der} {berufung} {der} {beklagte} gegen {der} {urteil}{textGroup} {wird} kostenpflichtig {zurückgewiesen}",
                         fr"{der} {berufung} {wird} auf Kosten {der} {beklagte} {zurückgewiesen}",
                         fr"{der} {beklagte} {hat} {der} {kosten} {der} {verfahrens} zu zahlen"]
#beider Verfahren

#Generate the string containing the "Kostenentscheid" from the content
#deprecated
def getKostenString(content):
    m = re.search(r"([\w\s\.äöü]*( trägt )[\w\s\.äöü]*( Kosten )[\w\s\.äöü]*)", content)
    text = ""
    if m != None:
        text = m.group()
    else:
        m = re.search(r"([\w\s\.äöü]*( Kosten )[\w\s\.äöü]*( trägt )[\w\s\.äöü]*)", content)
        if m != None:
            text = m.group()
        else:
            return ""
    return text
#"{der} Hälfte", "ein Drittel", "zwei Drittel", "\d\/\d", "¼", "¾"])
def getKostenLabel(tenor):
    if tenor == None or tenor == "":
        return None
    m = re.search(kostenaufteilungSentence, tenor)
    if m != None and m.groups() != None:
        l =  list(filter(lambda a: a != None, m.groups()))
        if l != []:
            beklagteCost = l[0]
        else:
            return None
        m2 = re.match(r"(\d{1,2}(?:,\d{1,2})?)[ ]*(?:(?:%)|(?:Prozent))", beklagteCost)
        if m2 != None:
            return float(m2.group(1).replace(",", ".")) * 0.01
        m2 = re.match(rf"{der} Hälfte", beklagteCost)
        if m2 != None:
            return 0.5
        m2 = re.match(rf"zur Hälfte", beklagteCost)
        if m2 != None:
            return 0.5
        m2 = re.match("ein Drittel", beklagteCost)
        if m2 != None:
            return 0.333
        m2 = re.match("einem Drittel", beklagteCost)
        if m2 != None:
            return 0.333
        m2 = re.match("zwei Drittel", beklagteCost)
        if m2 != None:
            return 0.666
        m2 = re.match("ein Fünftel", beklagteCost)
        if m2 != None:
            return 0.2
        m2 = re.match("vier Fünftel", beklagteCost)
        if m2 != None:
            return 0.8
        m2 = re.match("¾|(drei Viertel)", beklagteCost)
        if m2 != None:
            return 0.75
        m2 = re.match("¼|(ein Viertel)", beklagteCost)
        if m2 != None:
            return 0.25        
        m2 = re.match("(\d{1,3})\/(\d{1,3})(?:tel)?", beklagteCost)
        if m2 != None:
            return int(m2.group(1))*1.0/int(m2.group(2))
        return None
    for s in klägerKostenSentences:
        m = re.search(s, tenor)
        if m != None:
            return 0
    for s in beklagteKostenSentences:
        m = re.search(s, tenor)
        if m != None:
            return 1
    return None


def containsTatbestand(text):
    tatbestandHead = r"((Tatbestand)|(T a t b e s t a n d))[\s:]*\n"
    m = re.search(tatbestandHead,text)
    return m != None 

def stripContent(content, removeNr = True):
    #Tags removal
    text = re.sub('<[^<]+>', " ", content).strip()
    text = re.sub(r"&#([\d]*);", lambda x : chr(int(x.group()[2:-1])), text)
    #Whitespace remoal
    text = text.replace("\xa0", " ")
    text = re.sub("(\n)+", "\n", text)
    text = re.sub("(\t)+", "\t", text)
    text = re.sub(" +", " ", text)
    #text = unicodedata.normalize("NFKD", text)
    # &#??? code conversion
    ## so, I've seen case where characters like ä,ö,ü are written each as string
    #which is '&#' followed by the decimal character code, followed by ';'
    #the above substitution finds those string snippets and replaces them with the correct unicode character
    lines = text.split("\n")
    new_lines = []
    for line in lines:
        line = line.strip()
        if removeNr:
            line = re.sub("^\d{1,2}[.]*[)]*[\s]*", "", line)
            #line = re.sub("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})[.]*[)]*[\s]*", "", line)
        if line != "":
            new_lines.append(line)
    return '\n'.join(new_lines)


teilkosten = lOG([r"\d{1,2}(?:,\d{1,2})?[ ]*(?:(?:%)|(?:Prozent))", "{der} Hälfte", "zur Hälfte",
                  "ein Drittel", "einem Drittel", "zwei Drittel", "\d{1,3}\/\d{1,3}(?:tel)?", "¼", "¾",
                  "ein Viertel", "drei Viertel", "ein Fünftel", "vier Fünftel"])

_und = lOG([" und", ","])
zu = r"zu(?:einem Anteil von)?"

#319592: Der Kläger hat 1/5, die Beklagte hat 4/5 der Kosten des Rechtsstreits zu tragen. #That's why we add an optional 'hat' below

zuxxProzentDerKläger = lOG([rf"{der} {kläger} {zu} {teilkosten}", rf"{zu} {teilkosten} {der} {kläger}"])
zuxxProzentDerBeklagte = lOG([rf"{der} {beklagte} {zu} ({teilkosten})", rf"{zu} ({teilkosten}) {der} {beklagte}"])
xxProzentDerKläger = lOG([rf"{der} {kläger} (?:{hat} )*{teilkosten}", rf"(?:{hat} )*{teilkosten} {der} {kläger}"])
xxProzentDerBeklagte = lOG([rf"{der} {beklagte} (?:{hat} )*({teilkosten})", rf"(?:{hat} )*({teilkosten}) {der} {beklagte}"])


xxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt = lOG([rf"{xxProzentDerKläger}{_und} {xxProzentDerBeklagte}",rf"{xxProzentDerBeklagte}{_und} {xxProzentDerKläger}"])
zuxxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt = lOG([rf"{zuxxProzentDerKläger}{_und} {zuxxProzentDerBeklagte}",rf"{zuxxProzentDerBeklagte}{_und} {zuxxProzentDerKläger}"])             
                                                 

trägtKlägerZuBeklagteZu = lOG([rf"{trägt} {zuxxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt}", 
                               rf"{hat} {zuxxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} zu tragen",
                               rf"fallen {zuxxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} zur Last",
                               rf"werden {zuxxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} auferlegt"])
trägtKlägerBeklagte = lOG([rf"{trägt} {xxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt}",
                           rf"{hat} {xxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} zu tragen",
                           rf"fallen {xxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} zur Last",
                           rf"werden {xxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} auferlegt"])

jeweils = lOG(["je", "jeweils"])
kostenaufteilungSentence = lOG([rf"{der} {kosten} {der} {verfahrens} {trägtKlägerZuBeklagteZu}",
                               rf"Von {der} {kosten} {der} {verfahrens} {trägtKlägerBeklagte}",
                               rf"Von {der} {kosten} {der} {verfahrens} {hat} {der} {beklagte} ({teilkosten}) zu tragen",
                               rf"Von {der} {kosten} {der} {verfahrens} {hat} {der} {kläger} {teilkosten}{_und} {der} {beklagte} ({teilkosten}) zu tragen",
                               rf"{xxProzentDerKlägerUndxxProzentDerBeklagteOderUmgekehrt} {der} {kosten} des {verfahrens}",
                               rf"{der} {beklagte} {trägt} {der} {kosten} {zu} ({teilkosten})",
                               rf"{der} {beklagte} {trägt} zu ({teilkosten}) {der} {kosten}",
                               rf"{der} {beklagte} {trägt} ({teilkosten}) {der} {kosten}",
                               rf"Von {der} {kosten} {der} {verfahrens} {trägt} {der} {beklagte} ({teilkosten})",
                               rf"{der} {kläger} {trägt} {teilkosten}{_und} {der} {beklagte} ({teilkosten}) {der} {kosten}",
                               rf"{der} {kosten} des {verfahrens} werden {der} {kläger} zu einem Anteil von {teilkosten}{_und} {der} {beklagte} zu einem Anteil von ({teilkosten}) auferlegt",
                               rf"{der} {beklagte} {trägt} ({teilkosten}){_und} {der} {kläger} {teilkosten}(?: von)? {der} {kosten}",
                               rf"{der} {kläger} {trägt} {teilkosten}{_und} {der} {beklagte} ({teilkosten})(?: von)? {der} {kosten}",
                               rf"{der} {kläger} und {der} {beklagte} {hat} {der} {kosten} {jeweils} ({teilkosten}) zu tragen.",
                               rf"{der} {kosten} {der} {verfahrens} {trägt} (?:{kläger} und {beklagte})|(?:die Parteien) {jeweils} ({teilkosten})",
                               rf"{der} {kläger} {trägt} {teilkosten} {der} {kosten} {der} {verfahrens}{_und} {der} {beklagte} ({teilkosten})",
                               rf"{der} {kosten} {der} {verfahrens} {hat} {der} {kläger} und {der} {beklagte} {jeweils} ({teilkosten}) zu tragen"])

kostenAufteilungen = [rf"{der} {kosten} des {verfahrens} {trägtKlägerZuBeklagteZu}"]
#319592: maybe make the classification more intelligent there
#Die Kosten des Rechtsstreits nach einem Streitwert in Höhe von bis zu 13.000,00 € trägt die Klägerin. 330270 276352....

"""excludingKosten = lOG(["%", "Prozent", r"\d/\d",
                       rf"hat{sentenceGroup}Hälfte{sentenceGroup}zu tragen",
                   "nach einem ((Streitwert)|(Kostenwert))", 
                   rf"Kosten{sentenceGroup}Staatskasse{sentenceGroup}auferlegt",
                   rf"Kosten{sentenceGroup}werden gegeneinander aufgehoben",
                   r"nach einem ((Streitwert)|(Kostenwert))",
                   fr"bleibt {der} ((Schlussurteil)|(Endurteil)|(Schlussentscheidung)) vorbehalten",
                   r"Entscheidung ergeht gerichtsgebührenfrei",
                   fr"Antrag{sentenceGroup}auf Urteilsergänzung wird{sentenceGroup}abgewiesen",
                   r"Entscheidung ergeht gerichtsgebühren- und auslagenfrei",
                   fr"Berufung{sentenceGroup}gegen das Urteil",
                   fr"wird dahingehend abgeändert"])"""
neuen = lOG(["neuen", "erneuten", "anderweitigen"])
excludingKosten = lOG([rf"({kläger}|(Beteiligte)) zu (\d\)|\d.)", rf"{beklagte} zu (\d\)|\d.)", "nach einem Streitwert",
                       rf"werden gegeneinander aufgehoben",
                       rf"{der} Kostenentscheidung bleibt {der} ((Schlussurteil)|(Endurteil)|(Schlussentscheidung)) vorbehalten",
                       rf"Die Entscheidung über die Kosten des Rechtsstreits bleibt der Schlussentscheidung vorbehalten",
                      rf"Die Kläger haben je zur Hälfte die Kosten des Rechtsstreits zu tragen",
                      "Von den Kosten des Rechtstreits haben die Kläger zu je",
                      rf"Die Kläger haben die Kosten des Rechtsstreits zu je {teilkosten} zu tragen.",
                      rf"Die Kosten des {verfahrens} werden {der} {kläger} zu jeweils {teilkosten} auferlegt",
                      rf"tragen {der} {kläger} {teilkosten} und {der} {kläger} gesamtschuldnerisch mit {der} {beklagte}",
                      "Der Antragsgegner hat auch die weiteren Kosten des Verfahrens nach einem Kostenwert von 20.000,00 Euro zu tragen.",
                      "Widerbeklagten", "gerichtsgebührenfrei", "Urteilsergänzung", "Versäumnisurteil",
                      "gerichtsgebühren- und auslagenfrei", rf"Die Sache wird zur {neuen} Verhandlung{textGroup}zurückverwiesen",
                      rf"{der} (Sache|Rechtsstreit){textGroup}zur {neuen} Verhandlung{textGroup}zurückverwiesen", "erster Instanz",
                      "zweiten Instanz", "zweiter Instanz", "erstinstanzlichen", "2. Instanz"])#, rf"Die Berufung {der} ({kläger}|{beklagte})",
                      #rf"Die Berufung wird auf Kosten"])

def costExclusion(tenor):
    if tenor == "" or tenor == None:
        return True
    else:
        m = re.search(excludingKosten, tenor)
        return m != None
    

def getTenor(text):
    tenorHead = r"((Tenor)|T e n o r)[\s:]*"
    tatbestandHead = r"((Tatbestand)|(T a t b e s t a n d))[\s:]*\n"
    start = re.search(tenorHead, text)
    end = re.search(tatbestandHead, text)
    if start != None and end != None:
        return text[start.end(0):end.start(0)].strip()
    else:
        return None

def getTatbestand(text):
    tatbestandHead = r"((Tatbestand)|(T a t b e s t a n d))[\s:]*\n"
    gründeHead = r"((Entscheidungsgründe)|(E n t s c h e i d u n g s g r ü n d e)|(Gründe)|(G r ü n d e))"
    start= re.search(tatbestandHead,text)
    tg = textGroup
    end = re.search(fr"{gründeHead}[\s\d\:I.]*((Die{tg}Klage{tg}begründet.)|(Die{tg}Klage{tg}Erfolg.))", text)
    if start != None and end != None:
        return text[start.end(0):end.start(0)].strip()
    else:
        return None
    #m = re.search(r"((T a t b e s t a n d)|(Tatbestand))(.*)((Entscheidungsgründe)|(E n t s c h e i d u n g s g r ü n d e))",
    #              text)
    #if m != None:
    #    return m.group(4)
    #else:
    #    return None

def getContentTextTenor(d):
    content = d['content']
    text = stripContent(content)
    tenor = getTenor(text)
    return content, text, tenor

#Data Preprocessing
#Get the file here: https://static.openlegaldata.io/dumps/de/2020-12-10/
def getLineCount(file):
    count = 0
    with open(file) as f:
        return sum(1 for line in f)    
    
import tqdm
#Total number of cases: 201825.
#I'm going to hardcode this, because counting the lines itself takes some time
#Unsupervised texts:
    
#TODO: 319475 no idea what doesn't work here    

manualLabels = {84154 : 0.25, 122163 : 0, 72456 : 0.5, 77021 : 1, 79425 : 1}
    
arbeitsCases = []
import numpy as np
def createData(overwrite = False, maxNr = 50000, allowErrors = 0):
    with open(p/"cases.jsonl") as f:
        count = 0
        errors = 0
        for i in tqdm.tqdm(range(201825)):
            line = f.readline()
            if count > maxNr:
                break
            d = json.loads(line)
            out_path = pout/(str(d["id"])+".txt")
            if (not out_path.exists() or overwrite) and not excludeCase(d):
                content = d["content"]
                text = stripContent(content) #removes the HTML tags, unecessary whitespace
                tenor = getTenor(text)                   
                data = {}
                label = getKostenLabel(tenor)
                if label == None and d['id'] in manualLabels.keys():
                    label = manualLabels[d['id']]
                data['label'] = float(np.around(label, 3)) if label != None else None
                data['facts'] = getTatbestand(text)
                data['court'] = d['court']['name']
                data['file_number'] = d['file_number']
                data['date'] = d['date']
                data['type'] = d['type']
                data['ecli'] = d['ecli']
                data['tenor'] = tenor
                data['jurisdiction'] = d['court']['jurisdiction']
                if data['jurisdiction'] == "Arbeitsgerichtsbarkeit":
                    arbeitsCases.append(d['id'])
                if data['label'] != None and data['facts'] != None and not costExclusion(tenor):
                    with open(out_path, "w", encoding="utf-8") as out:
                        json.dump(data, out, indent = 2, ensure_ascii = False)
                        count += 1
                elif data['label'] == None and not costExclusion(tenor) and containsTatbestand(text):
                    if errors < allowErrors:
                        errors += 1 
                    else:
                        pass
                else:
                    pass
    #            if x == None and containsTatbestand(content):
    #                openCaseHTML(d['id'])
    #                break"""

def getCaseFromId(id_nr):
    with open(p/"cases.jsonl") as f:
        while True:
            line = f.readline()
            if not line:
                break
            d = json.loads(line)
            if(d['id'] == id_nr):
                return d
def getCaseFromNr(nr):
    with open(p/"cases.jsonl") as f:
        for i in range(nr):
            line = f.readline()
            if not line:
                break
            d = json.loads(line)
            return d
            
def saveCaseContent(d, path):
    with open(path/(str(d['id'])+".txt"), "w", encoding = "utf-8") as out:
        out.write(stripContent(d['content']))

#11% ordentliche Gerichtsbarkeit
#10% Arbeit

createData()
#content, text, tenor = getContentTextTenor(d)

"""with open(p/"cases.jsonl") as f:
    while True and count < 12: #1000 samples should be enough for now
        line = f.readline()
        if not line:
            break
        d = json.loads(line)
        if isNotStraf(d) and isUr(d) and isOrdentlich(d):
            content = d["content"]
            count+=1
            lbl = getKostenLabel(content)
            x = getX(content)
            if lbl != None and x != None:
                with open(pout/str(lbl)/(str(d["id"])+".txt"), "w", encoding="utf-8") as out:
                    out.write(x)"""
#Die Kosten des Rechtsstreits werden der klagenden Partei auferlegt.         
gründeAnfang = ["Die Klage ist zulässig, aber nicht begründet.", "Die Klage ist zulässig und überwiegend begründet",
                "Die zulässige Klage ist nicht begründet.", "Die Klage ist zulässig und begründet.",
                "Die Klage ist nicht begründet.", "Die zulässige Klage ist unbegründet."]
#Es ist nicht notwendig, dass nach (Entscheidungs)Gründe der erste Satz einer der obigen ist, siehe
#https://de.openlegaldata.io/case/ag-munchen-2020-10-27-473-c-213820
#https://de.openlegaldata.io/case/lg-wurzburg-2020-10-23-1-hk-o-125020

           
####Subselection:###
#ordentliche Gerichtsbarkeit
#kein Strafgericht
#Urteil, Endurteil
#entsprechend der X und Kosten-Masken (s. oben)