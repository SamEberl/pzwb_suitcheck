# Risikoanalyse

Mockup: https://app.moqups.com/iNGmHPrUSN/view/page/ad64222d5<br />
Präsentation: https://www.youtube.com/watch?v=aaKQHFyGM9o

## Projekt Charta
![Project Charta](Planning/SuitCheck_Project-Charta.png)

## Deliverable Struktur
![PBS](Planning/SuitCheck_Deliverables.png)

## Erstellung AWD-LSTM Model
![Gantt Diagram](Planning/SuitCheck_AWD-LSTM.png)
